#!/usr/bin/env perl
# Autoflush
$|++;

#####
# pictone - convert images to tones
#####
# License: ISC (http://opensource.org/licenses/ISC)
#
# Copyright (c) 2013, Szymon 'polemon' Bereziak <polemon@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this
# software for any purpose with or without fee is hereby granted,
# provided that the above copyright notice and this permission
# notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
# NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#####

use Math::Trig;
use Image::Magick;
use Getopt::Std;
use Audio::Wav;
use version;
use strict;

###
### Constants
###

# Version
our $VERSION = 0.02;

# Getopt settings
$Getopt::Std::STANDARD_HELP_VERSION = 1;

# low frequency 
my $lowfreq = 100;
# high frequency:
my $highfreq = 18000;
# Sample rate for CD quality
my $samplerate = 44100;

# frequency bandwith spacing:
# autoscale if =0
my $interval = 0;

###
### Default settings
###

my $pixelsecond = 15;
my $highamp = 600;
my $inverse = 0;
my $input;
my $output;

*PR_FH = *STDOUT;

### Parse CLI args
($input, $output, $pixelsecond, $highamp, $inverse, $interval, $highfreq) = parseArgs();

# Open image
my $img = new Image::Magick;
$img->Read($input);

# Create WAV file handle:
my $wav = new Audio::Wav;

my $header = {
    'bits_sample' => 16,
    'sample_rate' => $samplerate,
    'channels'    => 1,
};

print PR_FH "ofile: $output\n";
my $wh = $wav->write($output, $header);



# Get width and height
my $width = $img->Get('columns');
my $height = $img->Get('rows');

print PR_FH "Input: $input \n";
print PR_FH " size: ${width}px x ${height}px\n";
print PR_FH " +++ Image inverted +++\n" if ($inverse);
print PR_FH "Output: $output \n";
print PR_FH " pixelrate: $pixelsecond \n";
print PR_FH " amplitude: $highamp \n";

# scale frequency per height;
my $freqrange = $highfreq - $lowfreq;
$interval = $freqrange / $height if($interval == 0);

print PR_FH " Range    : ${lowfreq}Hz - ${highfreq}Hz (bandwidth: ${freqrange}Hz)\n";
print PR_FH " Intervals: ${interval}Hz\n";

my @frequencies;
for(my $x = $lowfreq; $x < $highfreq; $x += $interval) {
    unshift(@frequencies, $x);
}

print PR_FH "Sampling rate: $samplerate\n";

# Calculate samples per pixel
my $samplespixel =  int($samplerate / $pixelsecond);  
print PR_FH " Samples / pixel: $samplespixel \n";

# claculate total ammount of samples
my $totalsamples = $samplerate * $width / $pixelsecond;
print PR_FH " total samples  : $totalsamples \n";

# calculate each PCM value (this is where ALL the magic happens...)
for(my $s = 0 ; $s < $totalsamples; $s++) {
    # row also identifies frequency
    my $pcm = 0;
    for(my $row = 0; $row < $height; $row++) {
        my $amp = int($highamp * $img->GetPixel(x => int($s / $samplespixel), y => $row, channel => 'gray') );
        # inverse if desired
        $amp = $highamp - $amp if($inverse);
        # shadow cutoff
        if($amp > $highamp * 0.25) {
            $pcm += sin( 2 * pi * $frequencies[$row] * ($s / $samplerate) ) * $amp;
        }
    }

    $wh->write($pcm);
    printf(PR_FH "%.2f%% done...\r", ($s / $totalsamples) * 100);
}

$wh->finish();


# Read arguments from the command line
sub parseArgs {
    #Set options for reading
    my %args = ();
    getopts("i:o:p:a:b:m:hI",\%args);
    
    help() if($args{h}); 
    $input = (!$args{i}) ? "-" : $args{i};

    $output = ( (!$args{o}) or ($args{o} eq "-") ) ? "/dev/stdout" : $args{o};
    *PR_FH = *STDERR if( (!$args{o}) || ($args{o} eq "-") );

    $pixelsecond = $args{p} if($args{p}); 
    $highamp = $args{a} if($args{a}); 
    $inverse = 1 if($args{I});
    $interval = $args{b} if($args{b});
    $highfreq = $args{m} if($args{m});

    print "hallo" if($args{help});
    
    return ($input, $output, $pixelsecond, $highamp, $inverse, $interval, $highfreq);
}

# Display help message for program
sub help {
    my ($message) = @_;
    print <<EOF;
pictone version $VERSION by Szymon 'polemon' Bereziak <polemon\@gmail.com>.
Encodes any kind of image to a series of sine waves.
Inspectable with a spectrogram.

-----------------------------------------------------------------
Usage: pictone -i <image> -o <output.wav>

  -i [File]	Input image file (smaller and less colors is better)
  -o [File]	Output file (WAV 44.1khz 16bit mono)
  -p [int]	Pixels per Second (default: $pixelsecond)
  -a [int]	Max amplitude per sample (default: $highamp) Max: 32000
  -m [int]      Max frequency (default: 18000) Max: 22050
  -b [int]      Bandwidth spacing interval (default: 0 (=auto))
  -I		Invert image color (Best for light images)
  -h		Display this help message
-------------------------------------------------------------------
EOF
    print "Error: $message \n" if($message ne '');
    exit(1);
}

sub HELP_MESSAGE {
    help();
}

sub VERSION_MESSAGE {
    print <<EOF;
pictone version $VERSION, © 2013 Szymon 'polemon' Bereziak <polemon\@gmail.com>.
   - use `pictone -h` for usage and help.
EOF
exit(0);
}
